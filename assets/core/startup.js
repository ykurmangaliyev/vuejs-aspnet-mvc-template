﻿/* Imports */
import Vue from "vue";
Vue.config.debug = true;
Vue.config.devtools = true;

/* Bootstrap */
import BootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
Vue.use(BootstrapVue); 

/* VueResource */
import VueResource from "vue-resource";
Vue.use(VueResource);

/* App */
import App from "./app.vue";

import componentSidebar from "../component/sidebar.vue";
Vue.component("kb-sidebar", componentSidebar);

import pageHome from "../page/home.vue";
import pageError from "../page/error.vue"; 
import pageOrganizations from "../page/organizations.vue"; 
 
/* Router */   
import VueRouter from "vue-router"
const router = new VueRouter({ 
    routes: [ 
        {
            path: "/",
            component: pageHome
        },
        {
            path: "/organizations",
            component: pageOrganizations
        },
        { 
            path: "/error/:code", 
            component: pageError
        },
        { 
            path: "*",
            redirect: "/"
        }
    ]
});
Vue.use(VueRouter);

/* Instance */
const vue = new Vue({
    router,
    render: h => h(App),
    created() {
        console.log("created");
    },
    mounted() {
        console.log("mounted");
    }
}).$mount("#app");