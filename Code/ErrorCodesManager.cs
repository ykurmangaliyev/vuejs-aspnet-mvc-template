﻿using System.Collections;
using System.Web;
using System.Xml;
using NLog;
using Telebank2.Corporate.Entities.Framework;

namespace Kaspi.Business.WebOperator.Code
{
    /// <summary>
    /// Summary description for ResultManager
    /// </summary>
    public class ErrorCodesManager
    {
        private const string ObjectKey = "ApplicationErrorCodes";
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();


        public static string GetMessage(ResultCode resultCode)
        {
            return GetMessage((int)resultCode);
        }

        public static string GetMessage(int resultCode)
        {
            string message;
            Hashtable hashtable = GetErrorsList(PortalSession.Current.Locale.Name);

            if (hashtable.ContainsKey(resultCode.ToString()))
            {
                message = hashtable[resultCode.ToString()].ToString();
            }
            else
            {
                message = hashtable["-999"].ToString();
                logger.Warn("Error message not found. Error code = {0}, locale={1}", resultCode, PortalSession.Current.Locale.Name);
            }            
            return message;
        }

        private static Hashtable GetErrorsList(string locale)
        {
            if (HttpContext.Current.Application[ObjectKey + locale] == null)
            {
                LoadErrors(locale);
            }
            return (Hashtable)HttpContext.Current.Application[ObjectKey + locale];
        }


        private static void LoadErrors(string locale)
        {
            Hashtable hashtable = new Hashtable();
            var xmlInterface = new XmlDocument();
            xmlInterface.Load("./Content/errormesages.config");
            
            foreach (XmlNode childNode in xmlInterface.SelectSingleNode("errors").SelectNodes("error"))
            {                
                string errorCode = childNode.Attributes["code"].Value;
                string errorMessage = childNode.SelectSingleNode(locale).InnerText;                
                hashtable.Add(errorCode, errorMessage);
            }

            HttpContext.Current.Application.Remove(ObjectKey + locale);
            HttpContext.Current.Application.Add(ObjectKey + locale, hashtable);
        }
    }
}