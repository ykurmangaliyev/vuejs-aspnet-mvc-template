﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Kaspi.Business.WebOperator.Code.Helpers;
using Telebank2.Corporate.Entities.Dtos.UserManagement;
using Telebank2.Corporate.Entities.UserManagement;

namespace Kaspi.Business.WebOperator.Code.ViewModels
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            CreateMap<OrganizationDto, SearchOrganizationViewModel>()
                .ForMember(x => x.Status, cfg => cfg.MapFrom(x => UserHelper.GetOrganizationStatusName(x.OrganizationStatus)))
                .ForMember(x => x.StatusIsActive, cfg => cfg.MapFrom(x => x.OrganizationStatus == OrganizationStatus.Valid));

            CreateMap<OrganizationDto, OrganizationPageViewModel>();
        }

        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ViewModelProfile>();
            });
            config.AssertConfigurationIsValid();
            return config.CreateMapper();
        }
    }
}