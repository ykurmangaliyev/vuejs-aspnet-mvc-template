﻿namespace Kaspi.Business.WebOperator.Code.ViewModels
{
    public class OrganizationPageViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsKaspiBusinessClient { get; set; }

        public string Idn { get; set; }
    }
}