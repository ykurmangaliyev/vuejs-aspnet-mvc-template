﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kaspi.Business.WebOperator.Code.ViewModels
{
    public class SearchOrganizationViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Idn { get; set; }

        public string Status { get; set; }

        public bool StatusIsActive { get; set; }
    }
}