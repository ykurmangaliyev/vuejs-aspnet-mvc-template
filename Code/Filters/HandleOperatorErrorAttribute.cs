﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rps.Telebank.Com;
using Rps.Telebank.Com.Error;
using Telebank2.Corporate.Entities.Framework;

namespace Kaspi.Business.WebOperator.Code.Filters
{
    public class HandleOperatorErrorAttribute : System.Web.Mvc.HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var exception = filterContext.Exception;

            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                base.OnException(filterContext);
                return;
            }

            if (exception is HostException hostException && hostException.ErrorCode == (int)CommonResultCode.Security_SessionNotValid)
            {
                PortalSession.Current.Close(true);
                filterContext.Result = new OperationJsonResult(false, $"Сессия истекла", new { refresh = true }, null);
                return;
            }

            filterContext.Result = new OperationJsonResult(false, exception.Message, null, null);
        }
    }
}