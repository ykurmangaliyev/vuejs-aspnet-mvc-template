﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telebank2.Corporate.Entities.Framework;

namespace Kaspi.Business.WebOperator.Code.Exceptions
{
    public class WebOperatorException : Exception
    {
        public WebOperatorException(string message) : base(message)
        {
        }

        public WebOperatorException(int resultCode) : this(ErrorCodesManager.GetMessage(resultCode))
        {
            
        }

        public WebOperatorException(ResultCode resultCode) : this((int)resultCode)
        {
        }
    }
}