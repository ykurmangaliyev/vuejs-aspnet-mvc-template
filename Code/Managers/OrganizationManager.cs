﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Kaspi.Business.WebOperator.Code.Exceptions;
using Kaspi.Business.WebOperator.Code.ViewModels;
using NLog;
using Rps.Telebank.Client;
using Telebank2.Corporate.AdminInterfaces;
using Telebank2.Corporate.Entities.Dtos.UserManagement;
using Telebank2.Corporate.Entities.Framework;

namespace Kaspi.Business.WebOperator.Code.Managers
{
    public class OrganizationManager
    {
        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
        private static readonly IMapper ViewModelMapper = ViewModelProfile.CreateMapper();

        public static SearchOrganizationViewModel[] SearchOrganizations(string query)
        {
            try
            {
                int code = GetAdminManager().FindOrganizations(query, out var organizations);

                if (code != (int) ResultCode.Ok)
                {
                    throw new WebOperatorException(code);
                }

                return ViewModelMapper.Map<SearchOrganizationViewModel[]>(organizations);
            }
            catch (Exception ex)
            {
                Logger.Error("[{0}] {1}", ex.GetType(), ex);
                throw;
            }
        }

        public static OrganizationPageViewModel FindByIdn(string idn)
        {
            var adminManager = GetAdminManager();

            bool isFromKaspiBusiness = true;
            int code = adminManager.GetOrganizationByIdn(idn, out var organization);
            if (code != (int) ResultCode.Ok)
            {
                isFromKaspiBusiness = false;
                code = adminManager.GetOrganizationFromIbso(idn, out organization);
                if (code != (int) ResultCode.Ok)
                {
                    return null;
                }
            }

            var pageViewModel = ViewModelMapper.Map<OrganizationPageViewModel>(organization);
            return pageViewModel;
        }

        private static IAdminManager GetAdminManager()
        {
            return PortalSession.Current.GetTelebankClient().CreateService<IAdminManager>("AdminManager");
        }
    }
}