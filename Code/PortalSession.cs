﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Web;
using NLog;
using Rps.Telebank.Client;
using Rps.Telebank.Com.Error;
using Rps.Telebank.Com.Security;
using Rps.Telebank.Com.Users;
using Telebank2.Corporate.Entities.Framework;
using Telebank2.Corporate.Entities.Security;
using Telebank2.Corporate.Interfaces;

namespace Kaspi.Business.WebOperator.Code
{
    public class PortalSession
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private PortalSession()
        {
            SessionObjects = new Hashtable();   
        }

        #region Members

        public Hashtable SessionObjects { get; }
        public SessionSecurityInfo SessionSecurityInfo { get; private set; }
        public Role UserRole { get; private set; }

        public CultureInfo Locale { get; } = CultureInfo.GetCultureInfo("ru-RU");

        public bool IsAuthenticated => SessionSecurityInfo != null;

        #endregion

        #region Methods

        public ITelebankClient GetTelebankClient(SecurityInfo securityInfo)
        {
            var telebankClient = new TelebankClient
            {
                IsTestVersion = false
            };

            try
            {
                telebankClient.RemoteAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception)
            {
                telebankClient.RemoteAddress = "::1";
            }

            telebankClient.Locale = "ru-RU";
            telebankClient.SecurityInfo = securityInfo;

            return telebankClient;
        }

        public ITelebankClient GetTelebankClient()
        {
            return GetTelebankClient(SessionSecurityInfo);
        }

        public int Open(string activeDirectoryUsername)
        {
            try
            {
                var activeDirectorySecurityInfo = new ActiveDirectorySecurityInfo
                {
                    Name = activeDirectoryUsername,
                    Type = ActiveDirectorySecurityInfoType.User
                };

                var userManager = GetTelebankClient(activeDirectorySecurityInfo).CreateService<IUserManager>("UserManager");
                int resultCode = userManager.OpenServiceSession(out var userRole, out var sessionSecurityInfo);

                if (resultCode == (int) ResultCode.Ok)
                {
                    UserRole = userRole;
                    SessionSecurityInfo = sessionSecurityInfo;
                    HttpContext.Current.Session.Timeout = sessionSecurityInfo.Duration > 0 ? sessionSecurityInfo.Duration : 20;
                }

                return resultCode;
            }
            catch (HostException ex)
            {
                Logger.Error("HostException[{0}] {1}", ex.GetType(), ex);
                return (int) ex.ErrorCode;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                return (int) ResultCode.GenericError;
            }
        }

        public void Close(bool localOnly)
        {
            try
            {

                if (!localOnly)
                {
                    if (SessionSecurityInfo != null && SessionSecurityInfo.SessionId > -1)
                    {
                        var frameworkManager = GetTelebankClient().CreateService<IUserManager>("UserManager");
                        frameworkManager.CloseSession(SessionSecurityInfo.SessionId);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                SessionSecurityInfo = null;

                SessionObjects.Clear();

                HttpContext.Current.Session.Abandon();
            }
        }

        #endregion

        #region Static methods

        public static PortalSession Current
        {
            get
            {
                var portalSession = (PortalSession)HttpContext.Current.Session["PortalSession"];
                return portalSession;
            }
        }

        public static void Initialize()
        {
            var session = new PortalSession();
            HttpContext.Current.Session["PortalSession"] = session;
        }

        #endregion
    }
}