﻿using System;
using Rps.Telebank.Com.Users;
using Telebank2.Corporate.Entities.Security;
using Telebank2.Corporate.Entities.UserManagement;

namespace Kaspi.Business.WebOperator.Code.Helpers
{
    /// <summary>
    /// Summary description for UserHelper
    /// </summary>
    public class UserHelper
    {
        public static string GetUserStatusName(UserStatus status)
        {
            string name;
            switch (status)
            {
                case UserStatus.Active:
                    name = "Активен";
                    break;
                case UserStatus.Initial:
                    name = "Предварительная регистрация";
                    break;
                case UserStatus.Blocked:
                    name = "Заблокирован";
                    break;
                case UserStatus.TemporarilyBlocked:
                    name = "Временно заблокирован";
                    break;
                case UserStatus.Disabled:
                    name = "Отключен";
                    break;
                case UserStatus.TemporarilyStoped:
                    name = "На подтверждении";
                    break;
                case UserStatus.Rejected:
                    name = "Отклонен";
                    break;
                case UserStatus.TerrorismInvolvement:
                    name = "Это террорист!";
                    break;
                case UserStatus.Approved:
                    name = "Одобрен";
                    break;
                default:
                    name = "Неопределенный";
                    break;
            }
            return name;
        }

        public static string GetMobileTokenStatusName(MobileTokenStatus status)
        {
            string name;

            switch (status)
            {
                case MobileTokenStatus.NotActivated:
                    name = "Не активирован";
                    break;
                case MobileTokenStatus.Active:
                    name = "Активен";
                    break;
                case MobileTokenStatus.BlockedCr:
                case MobileTokenStatus.BlockedStaticPwd:
                case MobileTokenStatus.BlockedMtm:
                case MobileTokenStatus.BlockedMac:
                case MobileTokenStatus.Blocked:
                    name = "Заблокирован";
                    break;
                case MobileTokenStatus.Archived:
                    name = "В архиве";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }

            return name;
        }

        public static string GetMobileTokenStatusLabelClass(MobileTokenStatus status)
        {
            string name;

            switch (status)
            {
                case MobileTokenStatus.NotActivated:
                    name = "label label-default";
                    break;
                case MobileTokenStatus.Active:
                    name = "label label-success";
                    break;
                case MobileTokenStatus.BlockedMac:
                case MobileTokenStatus.BlockedCr:
                case MobileTokenStatus.BlockedStaticPwd:
                case MobileTokenStatus.BlockedMtm:
                case MobileTokenStatus.Blocked:
                    name = "label label-danger";
                    break;
                case MobileTokenStatus.Archived:
                    name = "label label-warning";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }

            return name;
        }

        public static string GetProfileStatusName(UserProfileStatus status)
        {
            var name = string.Empty;

            switch (status)
            {
                case UserProfileStatus.Active:
                    name = "Активен";
                    break;
                case UserProfileStatus.KbUserExpired:
                    name = "Истек срок действия полномочий";
                    break;
                case UserProfileStatus.ExecutiveExpired:
                    name = "Истек срок действия полномочий Д.Л.";
                    break;
                case UserProfileStatus.Hidden:
                    name = "Скрыт";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("status", status, null);
            }

            return name;
        }

        public static string GetProfileStatusLabelClass(UserProfileStatus status)
        {
            var name = string.Empty;

            switch (status)
            {
                case UserProfileStatus.Active:
                    name = "label label-success";
                    break;
                case UserProfileStatus.KbUserExpired:
                    name = "label label-warning";
                    break;
                case UserProfileStatus.ExecutiveExpired:
                    name = "label label-warning";
                    break;
                case UserProfileStatus.Hidden:
                    name = "label label-danger";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("status", status, null);
            }

            return name;
        }

        public static string GetOrganizationStatusName(OrganizationStatus status)
        {
            string name;
            switch (status)
            {
                case OrganizationStatus.Valid:
                    name = "Активен";
                    break;
                case OrganizationStatus.TemporaryStopped:
                    name = "Ожидает подтверждения";
                    break;
                case OrganizationStatus.Blocked:
                    name = "Отключен";
                    break;
                //case OrganizationStatus.Blocked:
                //    name = "Отключен";
                //    break;
                default:
                    name = "Неопределен";
                    break;
            }
            return name;
        }

        public static string GetOrganizationStatusLabelClass(OrganizationStatus status)
        {
            string name;
            switch (status)
            {
                case OrganizationStatus.Valid:
                    name = "label label-success";
                    break;
                case OrganizationStatus.TemporaryStopped:
                    name = "label label-warning";
                    break;
                case OrganizationStatus.Blocked:
                    name = "label label-danger";
                    break;
                //case OrganizationStatus.Blocked:
                //    name = "label label-default";
                //    break;
                default:
                    name = "label label-default";
                    break;
            }
            return name;
        }

        public static string GetStatusLabelCss(UserStatus status)
        {
            string name;
            switch (status)
            {
                case UserStatus.Active:
                    name = "label label-success";
                    break;
                case UserStatus.Initial:
                    name = "label label-info";
                    break;
                case UserStatus.Blocked:
                case UserStatus.TerrorismInvolvement:
                    name = "label label-danger";
                    break;
                case UserStatus.Disabled:
                    name = "label label-default";
                    break;
                case UserStatus.TemporarilyStoped:
                    name = "label label-warning";
                    break;
                case UserStatus.TemporarilyBlocked:
                    name = "label label-default";
                    break;
                case UserStatus.Rejected:
                    name = "label label-default";
                    break;
                default:
                    name = "label label-default";
                    break;
            }

            return name;
        }

        public static string GetSignTypeName(OrganizationSignerType signType)
        {
            string name;
            switch (signType)
            {
                case OrganizationSignerType.A:
                    name = "Первая подпись";
                    break;
                case OrganizationSignerType.B:
                    name = "Вторая подпись";
                    break;
                case OrganizationSignerType.C:
                    name = "Третья подпись";
                    break;
                case OrganizationSignerType.None:
                    name = "Без подписи";
                    break;
                default:
                    name = "не определено";
                    break;
            }

            return name;
        }

        public static string GetAuthenticationTypeName(UserAuthenticationType authType)
        {
            string name;
            switch (authType)
            {
                case UserAuthenticationType.Default:
                    name = "Логин/пароль";
                    break;
                case UserAuthenticationType.Sms:
                    name = "Логин/пароль + SMS";
                    break;
                case UserAuthenticationType.Otp:
                    name = "OTP токен";
                    break;
                case UserAuthenticationType.DigitalSignature:
                    name = "ЭЦП";
                    break;
                default:
                    name = "не известно";
                    break;
            }

            return name;
        }
    }
}