﻿using System.Web;
using System.Web.Mvc;
using Kaspi.Business.WebOperator.Code.Filters;

namespace Kaspi.Business.WebOperator
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleOperatorErrorAttribute());
        }
    }
}
