﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Kaspi.Business.WebOperator.Code;
using KaspiBusiness.Back.Contracts;
using Rps.Telebank.Client;
using Rps.Telebank.Com.Users;
using Telebank2.Corporate.Entities.UserManagement;

namespace Kaspi.Business.WebOperator
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            TypeSearcher.GetInstance().AddAssembly(typeof(CorpUser).Assembly);
            TypeSearcher.GetInstance().AddAssembly(typeof(User).Assembly);
            TypeSearcher.GetInstance().AddAssembly(typeof(TmMerchant).Assembly);
        }

        protected void Session_Start()
        {
            PortalSession.Initialize();
        }
    }
}
