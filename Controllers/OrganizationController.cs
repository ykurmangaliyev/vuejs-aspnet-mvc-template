﻿using System;
using System.Web.Mvc;
using Kaspi.Business.WebOperator.Code;
using Kaspi.Business.WebOperator.Code.Managers;
using Telebank2.Corporate.Entities.Dtos.UserManagement;

namespace Kaspi.Business.WebOperator.Controllers
{
    [Authorize]
    public class OrganizationController : BaseController
    {
        public OperationJsonResult Search(string query)
        {
            var result = OrganizationManager.SearchOrganizations(query);
            return Operation(content: result);
        }

        public OperationJsonResult Get(string idn)
        {
            var result = OrganizationManager.FindByIdn(idn);
            return Operation(content: result);
        }
    }
}