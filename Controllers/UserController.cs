﻿using System.Web.Mvc;
using Kaspi.Business.WebOperator.Code;

namespace Kaspi.Business.WebOperator.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        public OperationJsonResult Me()
        {
            return Operation(content: new
            {
                name = User.Identity.Name,
                role = PortalSession.Current.UserRole.Description
            });
        }
    }
}