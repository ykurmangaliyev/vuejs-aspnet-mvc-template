﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using Kaspi.Business.WebOperator.Code;
using Telebank2.Corporate.Entities.Framework;

namespace Kaspi.Business.WebOperator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!PortalSession.Current.IsAuthenticated)
            {
                var windowsIdentity = User.Identity as WindowsIdentity;
                if (windowsIdentity == null || !windowsIdentity.IsAuthenticated || windowsIdentity.Groups == null)
                {
                    return RedirectToAction("Unauthorized", new
                    {
                        code = ResultCode.ActiveDirectory_NoIdentity
                    });
                }

                int openSessionResult = PortalSession.Current.Open(windowsIdentity.Name);

                switch (openSessionResult)
                {
                    case (int)ResultCode.Ok:
                        break;

                    default:
                        return RedirectToAction("Unauthorized", new
                        {
                            code = openSessionResult
                        });
                }
            }

            return View();
        }

        public ActionResult Unauthorized(ResultCode code = ResultCode.GenericError)
        {
            ViewBag.Error = ErrorCodesManager.GetMessage((int)code);
            return View();
        }
    }
}