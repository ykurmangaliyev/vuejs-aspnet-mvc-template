/// <binding ProjectOpened='Watch - Development' />
"use strict";

// Плагин для очистки выходной папки (bundle) перед созданием новой
const NotifierPlugin = require("webpack-notifier");
const path = require("path");

module.exports = {
    entry: "./assets/core/startup.js",
    output: {
        filename: "script.js",
        path: path.resolve(__dirname, "dist")
    }, 
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: "babel-loader",
                exclude: /node_modules/, 
                options: { 
                    presets: ["es2015"]
                }
            },
            {
                test: /\.vue$/,
                loader: "vue-loader"
            },
            {
                test: /\.css$/,
                loader: ["style-loader", "css-loader"]
            },
            {
                test: /\.less$/,
                loader: "less-loader"
            },
            {
                test: /\.(svg|eot|woff2?|ttf|png)/,
                loader: "file-loader"
            }
        ]
    },
    resolve: {
      extensions: ["*", ".js", ".vue"]  
    },
    plugins: [
        new NotifierPlugin()
    ]
};